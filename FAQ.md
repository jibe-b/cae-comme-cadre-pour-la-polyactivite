# Qu'est-ce que la polyactivité ?

# Y a-t-il un lien avec le travail bénévole, si présent en France ?

# Suis-je obligé de passer par la CAE pour tous les travaux que je souhaite réaliser ?

# Puis-je continuer à rendre des services grauitement ?

# Comment est-il possible de s'entraider sans entrer dans la concurrence les uns avec les autres ?

# Si je propose mes services quelques heures par semaine pour quelque chose que d'autres personnes peuvent faire, est-ce que je ne risque pas de les mettre au chômage ?

# Y a-t-il un lien avec l'uberisation ?

# Y a-t-il un lien avec les entreprises plateforme capitalistes ?

# Est-ce que je peux me consacrer à une activité unique également ?

# Comment suis-je couvert par les assurances pour ces diverses activités ?

# Puis-je exercer n'importe quelle activité ?

# Toutes les CAE polyactivité permettent-elles de faire tous les types d'activité possible ?

# Est-ce de la polyactivité si la CAE se concentre sur un nombre réduit d'activités ?